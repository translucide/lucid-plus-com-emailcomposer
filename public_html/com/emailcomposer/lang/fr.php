<?php
define('EMAILCOMPOSER_COMPOSEFORM_TITLE','Envoi de courriels');
define('EMAILCOMPOSER_COMPOSE_FORM_FROM','De');
define('EMAILCOMPOSER_COMPOSE_FORM_FROMNAME','Nom de provenance');
define('EMAILCOMPOSER_COMPOSE_FORM_ADDTO','Récipients aditionnels');
define('EMAILCOMPOSER_COMPOSE_FORM_SELTO','Récipients');
define('EMAILCOMPOSER_COMPOSE_FORM_USERSCOUNT','%s utilisateur(s) sélectionné(s)');
define('EMAILCOMPOSER_COMPOSE_FORM_SENDBCC','Envoi en BCC');
?>
