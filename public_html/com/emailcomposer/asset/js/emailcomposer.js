/**
 * lucidEmailComposer
 *
 * Binds to email composer dialog and ensures proper validation.
 *
 * Options :
 *
 * dialog           The dialog element in which the email compose form will be displayed.
 * form             The form element (form tag) to bind the submit event to.
 * formContainer    The form container element for the submitSuccess
 *                  event. If omitted, assumes 'this' refers to the formContainer
 **/
(function( $ ) {
    var methods = {
        init: function(options) {
            var $this = $(this);
            options = $.extend({
                dialog: '#lucid_dialog',
                form: '#form',
                formContainer: this
            }, options);

            //Clear dialog on hide
            $(options.dialog).on('hidden.bs.modal', function (e) {
                $(e.target).removeData("bs.modal").find(".modal-content").empty();
            });

            //Submit button click handler
            $(options.form).find("button[type=submit]").on("click",function(e){
                $(options.formContainer).on("submitSuccess",function(){
                    console.info('submitSccess:');
                    console.info(options);
                    $(options.dialog).modal('hide');
                });
            });

            //Success handler.
            var successHandler = function(responseText, statusText, xhr, $form){
                console.info(responseText);
                console.info(options);
                if (responseText.success) {
                    $(options.formContainer).trigger("submitSuccess");
                }else {
                    $(options.formContainer).parent().children(".alert-error").remove();
                    $(options.formContainer).parent().append("<div class=\"alert alert-error\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a> "+responseText.message+"</div>");
                    $(options.formContainer).trigger("submitError");
                }
            };
            setTimeout(function(){
                $(options.form).ajaxForm({
                    success:successHandler
                });
            },500);
            $this.data('options', options);
            return this;
        },
    };
    $.fn.lucidEmailComposer = function(method) {
        if (methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.lucidEmailComposer' );
        }
    };
})(jQuery);
