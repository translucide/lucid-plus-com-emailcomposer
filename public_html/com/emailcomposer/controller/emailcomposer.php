
<?php
/**
 * Compose email
 *
 * Allows a user to compose emails
 *
 * June 26, 2014
 *
 * @version     0.1
 * @abstract
 * @package     kernel
 * @author      Translucide
 * @copyright   copyright (c) 2012 Translucide
 * @license
 * @since       0.1
 */

global $service;
$service->get('Ressource')->get('com/emailcomposer/lang/'.$service->get('Language')->getCode());
$service->get('Ressource')->get('com/emailcomposer/view');
$service->get('Ressource')->get('core/controller');
$service->get('Ressource')->get('core/display/form');
$service->get('Ressource')->get('core/model/profile');
$service->get('Ressource')->get('core/model/profilefield');

class EmailcomposerController extends Controller{
    private $eventSelection;

    public function __construct(){
        $this->component = 'emailcomposer';
    }

    public function execute(){
        global $service;
        $request = $service->get('Request')->get();
        $view = $this->getView();

        //Allow events to run before any API Call and modify $request
        $eventParams = $service->get('EventHandler')->trigger('emailcomposer.load',[
            'request' => $request,
            'view' => $view
        ]);

        return $eventParams['view'];
    }

    public function apiCall($op){
        global $service;

        //Allow events to run before any API Call and modify $request
        $eventParams = $service->get('EventHandler')->trigger('emailcomposer.beforeapicall',[
            'request' => $service->get('Request')->get(),
            'response' => []
        ]);

        $request = $eventParams['request'];
        $ret = $eventParams['response'];
        $op = (isset($request['op']))?$request['op']:'';

        //Build the view object if a view is specified.
        if (!$request['view']) $request['view'] = 'emailcomposer';
        $service->get('Ressource')->get('com/emailcomposer/'.strtolower(str_replace('..','',$request['view'])));
        $view = ucfirst(strtolower($request['view'])).'View';
        if (!class_exists($view)) {
            $view = 'EmailcomposerView';
        }
        $view = new $view();
        $view->setVar('request',$request);

        switch($op){
            /**
             * Loads a template into a dialog.
             */
            case 'loaddialog': {
                switch($request['view']) {
                    case 'compose' : {
                        $this->eventSelection = [];
                        $view->setVar('form',$this->getForm());
                    }break;
                }
                echo $view->render();
                exit;
            }break;

            /**
             * Receives the composer form data, queues the emails for
             * delivery.
             **/
            case 'send': {
                $this->eventSelection = [];
                $service->get('Ressource')->get('core/protocol/email');
                $email = new Email();
                $email->from($request['emailcomposer_from'], $request['emailcomposer_fromname']);

                //Send a copy to the sender.
                $email->to($request['emailcomposer_from'], $request['emailcomposer_fromname']);

                $users = $this->getToList();
                foreach($users as $v) {
                    $email->to($v['email'],$v['name']);
                }

                $email->viewRecipients(($request['emailcomposer_bcc'])?false:true);
                $email->subject($request['emailcomposer_subject']);

                //Buil email body
                $service->get('Ressource')->get('core/display/template');
                $tpl = new Template("core/display/template/email.tpl");
                $body = trim($request['emailcomposer_message']);
                if (!$body) $body = '---';
                $tpl->assign('content',$body);
                $tpl->assign('title',$request['emailcomposer_subject']);
                $email->html(true);
                $email->body($tpl->render());
                $ret = [
                    'success' => $email->queue()
                ];
            }break;
        }
        return $ret;
    }

    /**
     * Returns the corresponding URL in the language ID given
     * or simply returns the home page url if none found
     *
     * @public
     * @param string $url The url for which we need the translation
     * @param int $langid The language ID to find the URL for
     * @return string The translated URL in the language we asked for or false if not found.
     */
    public function translate($langid, $objid = 0, $url=''){
        global $service;
        return $service->get('Language')->getCodeForId($langid).'/emailcomposer/compose';
    }

    /**
     * Verifies if user has access to required capabilities to
     * send an email
     *
     * @access public
     * @param string $op Current operation being performed.
     * @return bool $hasAccess
     **/
    public function hasAccess($op){
        global $service;
        $request = $service->get('Request')->get();
        $isAdmin = $service->get('User')->isAdmin();
        if ($isAdmin) return true; // Admin has all rights.

        $hasAccess = false;
        if ($service->get('Capability')->can('core.email.send')) {
            $users = $service->get('Capability')->can('core.email.users');
            if (isset($request['emailcomposer_users']) && $users) {
                $hasAccess = true;
            }
        }

        //Allow events to run before any API Call and modify access
        $eventParams = $service->get('EventHandler')->trigger('emailcomposer.access',[
            'request' => $service->get('Request')->get(),
            'controller' => $this,
            'allowAccess' => $hasAccess
        ]);
        $hasAccess = $eventParams['allowAccess'];
        return $hasAccess;
    }

    public function getForm(){
        global $service;
        $request = $service->get('Request')->get();
        $form = new Form('api/emailcomposer/controller/emailcomposer/send', 'emailcomposer', '', 'POST');
        $form->setUseCase('popupdialog');
        $form->setVar('layout','vertical');

        //Users can be selected by specifying their email, user id or an event name
        //that will provide all names and emails in its response
        $users = (isset($request['users']))?implode(',',$request['users']):$request['emailcomposer_users'];
        $emails = (isset($request['emails']))?implode(',',$request['emails']):$request['emailcomposer_emails'];
        $eventdata = (isset($request['eventData']))?$request['eventData']:base64_decode($request['emailcomposer_eventdata']);
        if (is_array($eventdata)) $eventdata = json_encode($eventdata);

        $form->add(new HiddenFormField('emailcomposer_users',($users)?$users:''));
        $form->add(new HiddenFormField('emailcomposer_emails',($emails)?$emails:''));
        $form->add(new HiddenFormField('emailcomposer_eventdata',base64_encode(($eventdata)?$eventdata:'')));
        $form->add(new LabelFormField('emailcomposer_sendto',$this->getToSummary(),array(
            'title'=>EMAILCOMPOSER_COMPOSE_FORM_SELTO,
            'width' => 10,
        )));
        $form->add(new YesnoFormField('emailcomposer_bcc',1,array(
            'title'=>EMAILCOMPOSER_COMPOSE_FORM_SENDBCC,
            'width' => 2,
        )));
        $form->add(new TextFormField('emailcomposer_from',($request['emailcomposer_from'])?$request['emailcomposer_from']:$service->get('User')->getVar('email'),array(
            'title'=>EMAILCOMPOSER_COMPOSE_FORM_FROM,
            'width' => 6,
        )));
        $form->add(new TextFormField('emailcomposer_fromname',($request['emailcomposer_fromname'])?$request['emailcomposer_fromname']:$service->get('User')->getVar('name'),array(
            'title'=>EMAILCOMPOSER_COMPOSE_FORM_FROMNAME,
            'width' => 6,
        )));
        $form->add(new TextFormField('emailcomposer_subject',$request['emailcomposer_subject'],array(
            'title'=>_SUBJECT,
            'width' => 12,
        )));
        $form->add(new HtmleditorFormField('emailcomposer_message',($request['emailcomposer_message'])?$request['emailcomposer_message']:'',array(
            'title'=>_MESSAGE,
            'configfile' => 'email',
            'width' => 12,
            'rows' => 10
        )));
        return $form;
    }

    /**
     * Returns an array of emails and names to send the email to.
     *
     * Supports the users and emails settings.
     **/
    public function getToList() {
        global $service;
        $store = new UserStore();
        $crit = $this->getToCrit();
        $res = $store->get($crit,['asarray'=>true]);

        $res = array_merge($this->getEventSelection(),$res);

        if ($res) {
            return $res;
        }
        else {
            return null;
        }
    }

    /**
     * Returns a string summarizing selected users
     *
     **/
    public function getToSummary() {
        global $service;
        $count = $this->getToCount();
        $store = new UserStore();
        $crit = $this->getToCrit();
        $crit->setLimit(3);
        $res = $store->get($crit,['asarray'=>true]);

        //If we use an event selection instead, populate the results with
        //the beginning of the event selection
        if (count($res) < 3 && count($this->eventSelection) > 0) {
            for($i = 0; $i < 3; $i++) {
                if (isset($this->eventSelection[$i])){
                    $res[] = $this->eventSelection[$i];
                }
            }
        }

        $str = '';
        if ($count > 3) {
            $str .= sprintf(EMAILCOMPOSER_COMPOSE_FORM_USERSCOUNT,$count).': ';
        }
        foreach($res as $k => $v) {
            if ($k > 0) $str .= ', ';
            $str .= $v['name'];
        }
        if ($count > 3) {
            $str .= '...';
        }

        return $str;
    }

    /**
     * Get to count.
     **/
    public function getToCount() {
        $store = new UserStore();
        $count += $store->count($this->getToCrit());
        $count += count($this->getEventSelection());
        return $count;
    }


    /**
     * Returns the selection criteria for the TO field
     *
     * @return Criteria Selection criteria object
     **/
    public function getToCrit() {
        global $service;
        $request = $service->get('Request')->get();
        $store = new UserStore();
        $crit = new CriteriaCompo();
        $found = false;
        $critAdd = new CriteriaCompo();

        //Add users by ID
        if (isset($request['users']) && is_array($request['users'])) {
            $critAdd->add(new Criteria('user_id',$store->inClause($request['users']),'IN'),'OR');
            $found = true;
        }
        if (isset($request['emailcomposer_users'])) {
            $critAdd->add(new Criteria('user_id',$store->inClause(explode(',',$request['emailcomposer_users'])),'IN'),'OR');
            $found = true;
        }

        //Add users by email
        if (isset($request['emails']) && is_array($request['emails'])) {
            $critAdd->add(new Criteria('user_email',$store->inClause($request['emails']),'IN'),'OR');
            $found = true;
        }
        if (isset($request['emailcomposer_emails'])) {
            $critAdd->add(new Criteria('user_email',$store->inClause(explode(',',$request['emailcomposer_emails'])),'IN'),'OR');
            $found = true;
        }

        if (!$found) {
            //Scramble query...
            $crit->add(new Criteria('user_email','@#$@$@'));
        }
        else $crit->add($critAdd);

        $crit->setSelection('`user_email` as `email`,`user_name` as `name`');
        $crit->setOrder('ASC');
        return $crit;
    }

    /**
     * Get event selection
     *
     * @return array Selection, each record with name and email columns
     */
    public function getEventSelection(){
        global $service;
        $request = $service->get('Request')->get();

        //Do not reload selection for nothing.
        if (count($this->eventSelection) > 0) {
            return $this->eventSelection;
        }
        else {
            //Allow events to fill dest selection
            $eventdata = (isset($request['eventData']))?$request['eventData']:base64_decode($request['emailcomposer_eventdata']);
            $test = json_decode($eventdata,true);
            if ($test) $eventdata = $test;

            $eventParams = $service->get('EventHandler')->trigger('emailcomposer.selection',[
                'data' => $eventdata,
            ]);
            $this->eventSelection = $eventParams['selection'];
        }
        return $eventParams['selection'];
    }
}
?>
