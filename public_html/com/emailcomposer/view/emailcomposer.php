<?php
/**
 * Email composer view
 *
 * Jan 19, 2019
 *
 * @version     0.1
 * @abstract
 * @package     lucid-plus-com-emailcomposer
 * @author      Translucide
 * @copyright   copyright (c) 2012 Translucide
 * @license
 * @since       0.1
 */

global $service;
$service->get('Ressource')->get('core/view');

class EmailcomposerView extends DefaultView{
    public function init(){
    }

    public function render(){
        global $service;
        $service->get('Ressource')->get('core/display/template');
        $service->get('Ressource')->get('com/emailcomposer/asset/js/emailcomposer');

        $request = $this->getVar('request');
        $form = $this->getVar('form');

        $tpl = new Template('com/emailcomposer/template/emailcomposer-'.str_replace('..','',strtolower($request['view'])).'.tpl');
        $tpl->assign('formname','emailcomposer');
        $tpl->assign('form',$form->render());
        $tpl->assign('url',URL);
        $tpl->assign('dataurl',DATAURL);
        $tpl->assign('isDialog',($request['op'] == 'loaddialog')?1:0);
        $tpl->assign('isAdmin',$service->get('User')->isAdmin());

        $ret = $tpl->render();
        return $ret;
    }
}
?>
