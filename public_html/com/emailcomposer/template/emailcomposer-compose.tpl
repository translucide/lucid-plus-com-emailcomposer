{:if $isDialog:}
<form class="form-vertical" id="{:$formname:}" name="{:$formname:}" method="POST" action="{:$url:}en/api/emailcomposer/controller/emailcomposer/send">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">{:$smarty.const.EMAILCOMPOSER_COMPOSEFORM_TITLE:}</h4>
    </div>
    <div class="modal-body">
        <div id="{:$formname:}_div"><div class="warning"><noscript>Activate Javascript to view this form !</noscript></div></div>
        {:$form:}
    </div>
    <div class="modal-footer">
        <button type="cancel" class="btn btn-default btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> {:$smarty.const._CANCEL:}</button>
        <button type="submit" class="btn btn-default btn-primary"><span class="glyphicon glyphicon-floppy-open"></span> {:$smarty.const._SEND:}</button>
    </div>
</form>
{:else:}
<h1 class="modal-title">{:$smarty.const.EMAILCOMPOSER_COMPOSEFORM_TITLE:}</h1>
<form class="form-vertical" id="{:$formname:}" name="{:$formname:}" method="POST" action="{:$url:}en/api/emailcomposer/controller/emailcomposer/send">
    <div id="{:$formname:}_div"><div class="warning"><noscript>Activate Javascript to view this form !</noscript></div></div>
    {:$form:}
</form>
{:/if:}
<script langage="javascript" src="{:$url:}/js.js?v=1550534401&f[]=com%2Femailcomposer%2Fasset%2Fjs%2Femailcomposer.js&_=1550546691945"></script>
<script language="javascript" type="text/javascript">
$("#{:$formname:}").lucidEmailComposer({
    "dialog": "#lucidapp-dialog",
    "form": "#{:$formname:}",
    "formContainer": "#{:$formname:}_div"
});
</script>
