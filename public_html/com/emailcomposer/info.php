<?php
global $service;
$service->get('Ressource')->get('com/emailcomposer/'.$service->get('Language')->getCode().'.php');

/**
 * Collections of tasks to perform on install
 **/
$info = [
    'info' => [
        'title' => 'Email composer',
        'description' => 'Allows users to send emails.',
        'author' => 'Emmanuel Morin',
        'authorurl' => 'https://translucide.ca',
        'company' => 'Translucide',
        'companyurl' => 'https://translucide.ca',
        'url' => '',
        'version' => '0.1'
    ],
    'route' => [
    ],
    'setting' => [
    ],
    'settingcategory' => [
    ],
    'capability' => [
    ]
];
